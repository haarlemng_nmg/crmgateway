$(document).ready(function(){
    $.extend($.fn.validatebox.defaults.rules, {  
		userID: {  
			validator: function(value){
			    var reg = /^[a-zA-Z_0-9]+$/
		        return reg.test(value);  
			},  
			message: '登錄名必須為字符[A-Z]或數字'  
		}  
    }); 
	
	$.extend($.fn.validatebox.defaults.rules, {  
		validateText: {  
			validator: function(value){
			    var reg = /[<>]+/
		        return !reg.test(value);  
			},  
			message: '不能含有特殊字符<>'  
		}  
    }); 

	$.extend($.fn.validatebox.defaults.rules, {  
		number: {  
			validator: function(value){
			    var reg = /^[0-9]+$/
		        return reg.test(value);  
			},  
			message: '此項必須為數字'  
		}  
    }); 

	$.extend($.fn.validatebox.defaults.rules, {  
		date: {  
			validator: function(value){
			    var reg = /^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/
		        return reg.test(value);  
			},  
			message: '此項必須為yyyy-mm-dd格式的日期'  
		}  
    }); 

});

function formatDate() {
	var year=this.getFullYear();
	var month=this.getMonth()+1;
	var date=this.getDate();
	return year+"-"+month+"-"+date;
}
Date.prototype.formatDate=formatDate; 