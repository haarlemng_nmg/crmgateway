<?php
 
function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}

function arrayToObject($d) {
	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return (object) array_map(__FUNCTION__, $d);
	}
	else {
		// Return object
		return $d;
	}
}

function userDisplayName($user) {
    return $user['lastname'] . ',' . $user['firstname'] . ($user['nickname'] ? " (${user['nickname']})" : '');
}

function checkAuthByUrl($url)
{
	// 解析分组、模块和操作
	$url = trim($url, '/');
	$path = explode('/', $url);
	
	$action = array_pop($path);
	$module = array_pop($path);
	$appGroup = array_pop($path);

	import("@.ORG.Auth");
	return Auth::AccessDecision($appGroup, $module, $action);
}

function createMenu($modules) {
	$menus = array();
	foreach ($modules as $key => $row) {
		$items = $row['items'];
		if (!$items) continue;

		$displayItems = array();
		foreach ($items as $item) {
			if (!checkAuthByUrl($item['url'])) continue;
			$dependence = $item['dependence'];
			if (!$dependence) {
				$displayItems[] = $item;
				continue;
			}
			$accept = true;
			foreach ($dependence as $depends) {
				if (!checkAuthByUrl($depends)) {
					$accept = false;
					break;
				}
			}
			if ($accept) {
				$displayItems[] = $item;
			}	
		}

		if (!$displayItems) continue;

		$menus[$key]['items'] = $displayItems;
		$menus[$key]['logo'] = $row['logo'];
		$menus[$key]['height'] = 26 + 27*count($displayItems);
	}
	return $menus;
}

function microtime_format() {
    $time = number_format(microtime(true),8,'.','');
	return explode(".", $time);
}

function htmlspecialcharsx($str) {
	$s = htmlspecialchars($str);
	return str_replace('&amp;#', '&#', $s);
}
?>