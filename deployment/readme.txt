FULL installation (CRM Gateway)

1) git clone

cd /home/crmgateway/

git clone https://haarlemng_nmg@bitbucket.org/gitadmin/it-crm-gateway.git www


2) run the following command to create "Runtime"

cd /home/crmgateway/www/

sudo mkdir Runtime

sudo chown -R www-data:www-data Runtime

sudo chmod -R 775 Runtime

sudo rm -rf Runtime/*


3) update config.php

modify DB_USER and DB_PWD


4) setup schedule task to execute

start from 2014-04-01 08:00, every 4 hours (i.e. 00:00, 04:00, 08:00, 12:00, 16:00, 20:00)
