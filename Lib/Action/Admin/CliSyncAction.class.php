<?php
ini_set("memory_limit", -1);				// check
ini_set("soap.wsdl_cache_ttl", 1);

class CliSyncAction
{
	public function run() {
		import('@.ORG.SyncLogs');
		$cutOff_stamp = time() - (4*60*60);		// sync per 4 hours
		$cutOff_ymd = date('Y-m-d\TH:i:s\+08:00', $cutOff_stamp);
		
		// ---------- debug ----------------------
		$cutOff_stamp = strtotime('2014-08-05 00:00:00');										// test
		$cutOff_ymd = date('Y-m-d\TH:i:s\+08:00', strtotime('2014-08-05 00:00:00'));			// test
		
		
		// ---------- debug ----------------------
		
		SyncLogs::writeLog('N', APP_NAME . ' test ver ' . APP_VERSION);
		SyncLogs::writeLog('N', 'Sync Start, cut off: ' . $cutOff_ymd);
		
		$mySforceConnection = $this->connectSF();

		
		// ---- one time import ----	
		// $this->updateProductCir($mySforceConnection);					// product master manage by IT, ok
		// $this->updateAccountFromCir($mySforceConnection);				// mag subscriber, ok
		// $this->updateOpportunitiesFromCirOrder($mySforceConnection);		// mag subscription, ok

		// ---- periodically import ----
		 $this->updatePromotionCode($mySforceConnection, $cutOff_ymd);					// promotion code master, from ED CMS, ok
		 $this->updatePickupShop($mySforceConnection, $cutOff_ymd);						// pickup shop master, from ED CMS, ok
		
		// -- From ED CMS --
		// -- eMag
		 $this->updateAccountFromEDMember($mySforceConnection);								// ok
		 $this->updateOpportunitiesFromEDStoreInvoice($mySforceConnection, $cutOff_stamp);	// ok
		
		// -- Mag
		 $this->updateAccountFromEDSubscription($mySforceConnection, $cutOff_ymd);			// ok, not run in one time import
		 $this->updateOpportunitiesFromEDSubscription($mySforceConnection, $cutOff_ymd);	// ok, not run in one time import
		
		// -- eMag (promotion code)
		 $this->updateOpportunitiesFromEDPromotion($mySforceConnection, $cutOff_stamp);		// ok
		
		// -- Lead (member not in subscription and promotion)
		// $this->updateLeadFromEDMember($mySforceConnection, $cutOff_ymd);					// pending
		
		
		// -- Update Cirdb from CRM
		 $this->updateCirdbCustomer($mySforceConnection, $cutOff_ymd);						// ok
		 $this->updateCirdbOrderData($mySforceConnection, $cutOff_ymd);						// ok

		$this->updateCirdbPickupShop($mySforceConnection, $cutOff_ymd);

		$this->disconnectSF($mySforceConnection);
		
		/* remove the runtime files, else data with charset problem */
		foreach (glob(APP_PATH . "/Runtime/Data/_fields/*.php") as $filename) {
			unlink($filename);
			SyncLogs::writeLog('N', 'remove: ' . $filename, __FUNCTION__);
		}
		
		SyncLogs::writeLog('N', 'Sync End');
	}
	
	private function connectSF() {
		// -- make connection to Salesforce.com --
		try {
			$mySforceConnection = new SforceEnterpriseClient();
			$mySforceConnection->createConnection(FORCEPHP_PATH . SF_EWSDL);
			$mySforceConnection->login(SF_USERNAME, SF_PASSWORD.SF_SECURITY_TOKEN);	
			// $mySforceConnection->setAllOrNoneHeader(true);	 // dev

			SyncLogs::writeLog('N','login Salesforce.com');
		
			return $mySforceConnection;
		} catch (Exception $e) {
			// $this->writeLog('Exception @' . __FUNCTION__ . ' >>' . 'Salesforce Last Request: ' . $mySforceConnection->getLastRequest());
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function disconnectSF($mySforceConnection) {
		// -- disconnect from Salesforce.com --
		try {
			$mySforceConnection->logout();
			SyncLogs::writeLog('N','logout Salesforce.com');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updatePromotionCode($mySforceConnection, $cutOff) {
		// -- update Promotion Code --
		try {
			// Action on ED CMS //
			$EDPromoCode = D('EDPromoCode');
			$d = $EDPromoCode->getPromoCode($cutOff);
			
			// Action on SF //
			$SFPromoCode = D('SFPromoCode');
			$SFPromoCode->upsertPromoCode($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Promtion Code from ED CMS');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updatePickupShop($mySforceConnection, $cutOff) {
		// -- update Pickup Shop --
		try {
			// Action on ED CMS //
			$EDPickupShop = D('EDPickupShop');
			$d = $EDPickupShop->getPickupShop($cutOff);
			
			// Action on SF //
			$SFPickupShop = D('SFPickupShop');
			$SFPickupShop->upsertPickupShop($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Pickup Shop from ED CMS');

		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}	
	}
	
	private function updateProductCir($mySforceConnection) {
		// -- update Products from Cirdb --
		try {
			// Action on Cirdb //
			$CirPackage = D('CirPackage');
			$d = $CirPackage->getPackage();
			
			// Action on SF //
			$SFProduct = D('SFProduct');
			$SFProduct->upsertProduct($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Product from Cirdb');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}	
	}
	
	private function updateAccountFromEDSubscription($mySforceConnection, $cutOff) {
		// -- update Accounts from ED.subscription --
		try {
			// Action on ED //
			$EDSubscription = D('EDSubscription');
			$d = $EDSubscription->getEDSubscription($cutOff);
			
			// Action on SF //
			$SFAccount = D('SFAccount');
			$SFAccount->upsertAccountFromEDSubscription($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Account from ED Subscription');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updateOpportunitiesFromEDSubscription($mySforceConnection, $cutOff) {
		// -- update Opportunities from ED.subscription --
		try {
			// Action on ED //
			$EDSubscription = D('EDSubscription');
			$d = $EDSubscription->getEDSubscription($cutOff);
			
			// Action on SF //
			$SFOpportunities = D('SFOpportunities');
			$SFOpportunities->upsertOpportunitiesFromEDSubscription($mySforceConnection, $d);
				
			SyncLogs::writeLog('N', 'updated Opportunities from ED Subscription');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updateAccountFromEDMember($mySforceConnection) {
		// -- update Account from ED.member_ed --
		try {
			// Action on SF //
			$CRMGateway = D('SFCRMGateway');
			$uid = $CRMGateway->getUid($mySforceConnection, 'ED');
			
			// Action on ED //
			$EDMember = D('EDMember');
			$d = $EDMember->getMember($uid);
			
			// Action on SF //
			$SFAccount = D('SFAccount');
			$response = $SFAccount->upsertAccountFromEDMember($mySforceConnection, $d);
			
			$CRMGateway->updateUid($mySforceConnection, 'ED');
			
			SyncLogs::writeLog('N', 'updated Account from ED Member');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updateOpportunitiesFromEDStoreInvoice($mySforceConnection, $cutOff) {
		// -- update Opportunities from ED.store_invoie --
		try {
			// Action on ED //
			$EDStoreInvoice = D('EDStoreInvoice');
			$d = $EDStoreInvoice->getEDStoreInvoice($cutOff);
			
			// Action on SF //
			$SFOpportunities = D('SFOpportunities');
			$SFOpportunities->upsertOpportunitiesFromEDStoreInvoice($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Opportunities from ED Store Invoice');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}

	private function updateOpportunitiesFromEDPromotion($mySforceConnection, $cutOff) {
		// -- update Opportunities from ED.store_promotioncode_record --
		try {
			// Action on ED //
			$EDPromotion = D('EDStorePromotionRecord');
			$d = $EDPromotion->getEDStorePromotionRecord($cutOff);
			
			// Action on SF //
			$SFOpportunities = D('SFOpportunities');
			$SFOpportunities->upsertOpportunitiesFromEDPromotion($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Opportunities from ED Store Promotion');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updateLeadFromEDMember($mySforceConnection, $cutOff) {
		// -- update Lead from ED.member_ed -- // pending
	
	}
	
	private function updateAccountFromCir($mySforceConnection) {
		// -- update Account from Cirdb.customer --
		try {
			// Action on Cirdb //
			$CirCustomer = D('CirCustomer');
			$d = $CirCustomer->getCustomer();
			
			// Action on SF //
			$SFAccount = D('SFAccount');
			$SFAccount->upsertAccountFromCir($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Account from Cirdb');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updateOpportunitiesFromCirOrder($mySforceConnection) {
		// -- udpate Opportunities from Cirdb.orderdata --
		try {
			// Action on Cirdb //
			// get order data
			$CirOrder = D('CirOrderData');
			$d = $CirOrder->getOrderData();
			
			// Action on SF //
			$SFOpportunities = D('SFOpportunities');
			$SFOpportunities->upsertOpportunitiesFromCir($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Opportunities from Cirdb');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updateCirdbCustomer($mySforceConnection, $cutOff) {
		// -- update Cirdb.customers and customersdetails --
		try {
			// created on <datetime> and product family = 'EDWM'
			
			// get new Personal Account from SF
			$SFAccount = D('SFAccount');
			$d = $SFAccount->getPersonalAccountByModifyDate($mySforceConnection, $cutOff);		// EDWM, '2014-03-15T00:00:00+08:00'
			
			// update to Cirdb
			$CirCustomer = D('CirCustomer');
			$CirCustomer->insertCustomerFromPersonalAccount($d);

			// get new Business Account from SF
			$SFAccount = D('SFAccount');
			$d = $SFAccount->getBusinessAccountByModifyDate($mySforceConnection, $cutOff);		// EDWM, '2014-03-15T00:00:00+08:00'
			
			// update to Cirdb
			$CirCustomer = D('CirCustomer');
			$CirCustomer->insertCustomerFromBusinessAccount($d);

			// update CustNo to Cirdb from SF
			$CirCustNo = D('CirCustNo');
			$CirCustNo->updateCustNoFromSF($mySforceConnection);
			
			SyncLogs::writeLog('N', 'updated Customer to Cirdb');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}	
	}
	
	private function updateCirdbOrderData($mySforceConnection, $cutOff) {
		// -- update Cirdb.orderdata --
		try {
			// get new Opportunities from SF
			$SFOpp = D('SFOpportunityLineItem');
			$d = $SFOpp->getOpportunityLineItemByModifyDate($mySforceConnection, $cutOff);		// '2014-03-15T00:00:00+08:00'
			
			// update to Cirdb
			$CirOrderData = D('CirOrderData');
			$orderNo = $CirOrderData->insertOrderData($mySforceConnection, $d);
			
			// update to SF
			$crmGateway = D('SFCRMGateway');
			$crmGateway->updateOrderNo($mySforceConnection, $orderNo);
			
			SyncLogs::writeLog('N', 'updated Opportunities to Cirdb');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
	}
	
	private function updateCirdbPickupShop($mySforceConnection, $cutOff) {
		// -- update Cirdb.place --
		try {
			// get new Pickup Shop from SF
			$SFPickupShop = D('SFPickupShop');
			$d = $SFPickupShop->getPickupShopByModifyDate($mySforceConnection, $cutOff);
	
			// update to Cirdb
			$CirPlace = D('CirPlace');
			$place = $CirPlace->insertPlace($mySforceConnection, $d);
			
			SyncLogs::writeLog('N', 'updated Place to Cirdb');
		} catch (Exception $e) {
			SyncLogs::writeLog('E', $e->getMessage(), __FUNCTION__);
			exit;
		}
		

	}
}

?>