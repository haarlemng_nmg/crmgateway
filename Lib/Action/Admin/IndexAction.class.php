<?php

class IndexAction extends EntryAction {

    // 首页
    public function index() {
		if (!empty($_SERVER['argv']) && $_SERVER['argv'][1] == 'cli') {
			$CLI = new CliSyncAction();
			$CLI->run();
			exit;
		}
		
		echo 'Please contact your system administrator.';
    	
    	//$this->redirect('Admin/Auth');
    }
}

?>