<?php

class CirPlaceModel extends Model {
	protected $trueTableName = 'cirdb_place';
	
	// -- get Place by code
	public function getPlaceByCode($code) {
		C('DB_CHARSET','utf8');
	
		if ( substr($code,1,1) == 'S') { $place = substr_replace($code,'',2,3); }
			else { $place = $code; }
		
		// query
		$condition['place'] = $place;
		$d = $this->db(1, CIRDB_CONFIG)->where($condition)->select();
		
		if ( $d == null ) { SyncLogs::writeLog('E', 'Place "' . $place . '" not found'); }
		
		return $d;
	}
	
	public function checkPlace($code) {
		C('DB_CHARSET','utf8');
	
		$condition['place'] = $code;
	
		$d = $this->db(1, CIRDB_CONFIG)
				->field('count(place) ctn')
				->where($condition)->select();
	
		return $d;
	}
	
	// -- insert Place from SF
	public function insertPlace($sfConnection, $d) {
		C('DB_CHARSET','utf8');
		
		// update Place
		$data = D('CirPlace');
		
		for ($i=0; $i < $d->size; $i++) {
			$data->place = $d->records[$i]->place__c;
			$data->p_desc = $d->records[$i]->Shop_Address__c;
					
			$count = $this->checkPlace($d->records[$i]->place__c);
			if ( $count[0][ctn] == 0 ) {
				$result = $data->db(1, CIRDB_CONFIG)->add();
				
				if ( $result !== false ) { SyncLogs::writeLog('N', 'add Place to Cir: ' . $d->records[$i]->place__c, __FUNCTION__); }
					else { SyncLogs::writeLog('E', 'add Place to Cir: ' . $d->records[$i]->place__c . ', Query: ' . $data->getLastSql() , __FUNCTION__); }
			}
		}
	}
}

?>