<?php

class SFProductModel extends Model {

	// get one or a list of Product
	public function getProductById($sfConnection, &$ids) {
		echo 'SFProductModel getProduct' . PHP_EOL;
		
		// query
		$query = "SELECT Id, Name, ProductCode from Product2 where Id = " . $ids;
		$response = $sfConnection->query($query);
		
		foreach ($response->records as $record) {
			// echo $record->Id . ": " . $record->Name . " " . $record->ProductCode . PHP_EOL;
			array_push($ids, $record->Id);
		}
	}
	
	// get Product by Product Code
	public function getProductByProdCode($sfConnection, $ProdCode) {
		$query = "SELECT Id, Name, ProductCode from Product2 where ProductCode = '" . $ProdCode . "'";
		$response = $sfConnection->query($query);
		
		return $response;
	}
	
	// get Product by Plan Id
	public function getProductByPlanId($sfConnection, $PlanId) {
		$query = "SELECT Id, Name, ProductCode from Product2 where plan_id__c = " . $PlanId;
		$response = $sfConnection->query($query);
		
		return $response;
	}
	
	// upsert one or a list of Product
	public function upsertProduct($sfConnection, $d) {
		$sObject = new stdClass();
		
		for ($i=0; $i < count($d); $i++) {
			$sObject->duplicate_chk__c = $d[$i]["code"];
			$sObject->Family = $d[$i]["prod_code"];
			if ($d[$i]["cancel"] == 0) { $sObject->IsActive = 'true'; }
				elseif ($d[$i]["cancel"] == 1) { $sObject->IsActive = 'false'; }
			$sObject->Name = $d[$i]["desc"];
			$sObject->Period__c = $d[$i]["period"];
			$sObject->ProductCode = $d[$i]["code"];
			$sObject->Total_Issue__c = $d[$i]["total_issue"];
			
			$respone = $sfConnection->upsert('duplicate_chk__c', array($sObject), 'Product2');
			
			// if success, update PriceBookEntry
			if ($respone[0]->success == true)
			{
				$SFPriceBookEnty = D('SFPriceBookEntry');
				$SFPriceBookEnty->upsertPriceBookEntry($sfConnection, $d, $i, $respone[0]->id);
			}
			
			if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Product: ' . $d[$i]["code"] , __FUNCTION__);}
				else { SyncLogs::writeLog('E', 'upsert Product: ' . $d[$i]["code"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
		}
	}
	
	// delete one or a list of Product
	public function delProduct($sfConnection, $ids) {		
		$response = $sfConnection->delete($ids);

		return $response.success;
	}
}

?>