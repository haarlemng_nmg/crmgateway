<?php

class CirCustomerDetailsModel extends Model {
	protected $trueTableName = 'cirdb_customersdetails';

	public function getCustomerDetails() {
		C('DB_CHARSET','utf8');

		// query, one time import
		$condition['cirdb_customers.active'] = 1;
		
		$d = $this->db(1, CIRDB_CONFIG)
				->join('cirdb_customersdetails on cirdb_customers.cust_code = cirdb_customersdetails.cust_code')
				->field('cirdb_customers.*, cirdb_customersdetails.*')
				->where($condition)->select();
		
		return $d;
	}
	
	public function checkAccountNumber($accountNumber) {
		C('DB_CHARSET','utf8');
		
		$condition['cust_code'] = $accountNumber;
		
		$d = $this->db(1, CIRDB_CONFIG)
				->field('count(cust_code) ctn')
				->where($condition)->select();

		return $d;
	}
	
	public function insertCustomerDetails($d, $i, $personal) {				
		// update customer details
		$data = D('CirCustomerDetails');
		
		if ( $personal == true ) {
			$data->cust_code = $d->records[$i]->AccountNumber;
			$data->email = $d->records[$i]->PersonEmail . '';
			$data->hkid = $d->records[$i]->HK_ID__c . '';
			$acc = $d->records[$i]->AccountNumber;
		} else {
			$data->cust_code = $d->records[$i]->Account->AccountNumber;
			$data->email =  $d->records[$i]->Email . '';
			$acc = $d->records[$i]->Account->AccountNumber;
		}
		
		$count = $this->checkAccountNumber($acc);
		if ( $count[0][ctn] == 0 ) { 
			$result = $data->db(1, CIRDB_CONFIG)->add(); 
			
			if ( $result !== false ) { SyncLogs::writeLog('N', 'add Customer Detail to Cir: ' . $acc, __FUNCTION__); }
				else { SyncLogs::writeLog('E', 'add Customer Detail to Cir: ' . $acc . ', Query: ' . $data->getLastSql() , __FUNCTION__); } 
		} else { 
			$result = $data->db(1, CIRDB_CONFIG)->save(); 
			
			if ( $result !== false ) { SyncLogs::writeLog('N', 'update Customer Detail to Cir: ' . $acc, __FUNCTION__); }
				else { SyncLogs::writeLog('E', 'update Customer Detail to Cir: ' . $acc . ', Query: ' . $data->getLastSql() , __FUNCTION__); }
		}
	}
	
}

?>