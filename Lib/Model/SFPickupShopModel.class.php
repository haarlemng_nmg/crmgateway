<?php

class SFPickupShopModel extends Model {

	// get one or a list of Pickup Shop
	public function getPickupShop($sfConnection, &$ids) {		
		// query
		$query = "SELECT Id, Name, Shop_Category__c, Shop_Address__c from Pickup_Shop__c limit 200";
		$response = $sfConnection->query($query);
		
		foreach ($response->records as $record) {
			// echo $record->Id . ": " . $record->Name . " " . $record->Shop_Category__c . " " . $record->Shop_Address__c . PHP_EOL;
			//array_push($id, $record->Id);
			
			// echo $record->Name . ' ';
			
			// $r = $sfConnection->delete($record->Id);
			// echo $r[0]->success . PHP_EOL;
		}
	}
	
	// get Pickup Shop by Shop Code
	public function getPickupShopByCode($sfConnection, $ShopCode) {
		// query
		$query = "SELECT Id, Name, Shop_Category__c, Shop_Address__c, Place__c from Pickup_Shop__c WHERE Name = '" . $ShopCode . "'";
		$response = $sfConnection->query($query);
		
		return $response;
	}
	
	// get Pickup Shop by Shop Code (place)
	public function getPickupShopByPlace($sfConnection, $Place) {
		// query
		$query = "SELECT Id, Name, Shop_Category__c, Shop_Address__c, Place__c from Pickup_Shop__c WHERE Place__c = '" . $Place . "'";
		$response = $sfConnection->query($query);
	
		return $response;
	}
	
	// get Pickup Shop by Modify Date
	public function getPickupShopByModifyDate($sfConnection, $mDate) {
		// query
		$query = "SELECT Id, Name, Shop_Category__c, Shop_Address__c, Place__c  
					FROM Pickup_Shop__c
					WHERE IsDeleted = false
					AND LastModifiedDate > " . $mDate;
		$response = $sfConnection->query($query);
		
		return $response;
	}
	
	// upsert one or a list of Pickup Shop
	public function upsertPickupShop($sfConnection, $d) {
		$sObject = new stdClass();
		
		for ($i=0; $i < count($d); $i++) {
			$sObject->Name = $d[$i]["code"];
			$sObject->Shop_Address__c = $d[$i]["address"];
			$sObject->Shop_Category__c = $d[$i]["cname"];
			if ($d[$i]["status"] == 0) { $sObject->Valid__c = 'Invalid'; }
				elseif ($d[$i]["status"] == 1) { $sObject->Valid__c = 'Valid'; }
			$check = substr($d[$i]["code"], 0, 4);
			
			if ( $check == 'SEV-') { 
				$new = substr_replace($d[$i]["code"], '', 1, 3);
				$sObject->place__c = $new; 
			}
				else { $sObject->place__c = $d[$i]["code"]; }

			$respone = $sfConnection->upsert('Name', array($sObject), 'Pickup_Shop__c');
			
			if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Pickup Shop: ' . $d[$i]["code"] , __FUNCTION__);}
				else { SyncLogs::writeLog('E', 'upsert Pickup Shop: ' . $d[$i]["code"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
		}
	}
	
	// delete one or a list of Pickup Shop
	public function delPickupShop($sfConnection, $ids) {		
		$response = $sfConnection->delete($ids);

		return $response.success;
	}
}

?>