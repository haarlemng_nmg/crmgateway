<?php

class SFContactModel extends Model {

	public function getContact($sfConnection) {
		echo 'SFContactModel getContact' .PHP_EOL;
		
		// query and printout
		$query = "SELECT Id, FirstName, LastName, Phone from Contact limit 5";
		$response = $sfConnection->query($query);      // ******
		
		// var_dump($response);
		
		echo "Results of query '$query'" .PHP_EOL;
		foreach ($response->records as $record) {
			echo $record->Id . ": " . $record->FirstName . " "
					. $record->LastName . " " . $record->Phone .PHP_EOL;
		}
	}
	
	// update Contact of Business Account from Cirdb
	public function upsertBusinessContactCir($sfConnection, $d, $i, $AccountId) {
		$sObject = new stdClass();
		
		$sObject->AccountId = $AccountId;			// compulsory
		$sObject->cirdb_cust_code__c = $d[$i]["cust_code"];
		$sObject->Email = $d[$i]["email"];
		$sObject->LastName = $d[$i]["contact"];		// compulsory
		$sObject->MobilePhone = $d[$i]["telnext2"];
		$sObject->Phone = $d[$i]["telnext1"];
		if ($d[$i]["greet"] == 'Mr.' or $d[$i]["greet"] == '先生') { $sObject->Salutation = 'Mr.'; }
			elseif ($d[$i]["greet"] == 'Mrs.' or $d[$i]["greet"] == '女士') { $sObject->Salutation = 'Mrs.'; }
			elseif ($d[$i]["greet"] == 'Ms.' or $d[$i]["greet"] == 'Miss' or $d[$i]["greet"] == '小姐') { $sObject->Salutation = 'Ms.'; }
			elseif ($d[$i]["greet"] == 'Dr.') { $sObject->Salutation = 'Dr.'; }
			
		$Contact = $sfConnection->upsert('cirdb_cust_code__c', array($sObject), 'Contact');			
	}
}

?>