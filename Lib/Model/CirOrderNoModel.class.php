<?php

class CirOrderNoModel extends Model {
	protected $trueTableName = 'cirdb_orderno';
	
	public function getOrderNo() {
		C('DB_CHARSET','utf8');
		
		// get next order no from Cirdb
		$orderno = $this->db(1, CIRDB_CONFIG)->select();
		
		return $orderno[0]["idx"];
	}
	
	public function updateOrderNo($newOrderNo) {
		C('DB_CHARSET','utf8');
		
		// update new order no to Cirdb
		$data['idx'] = $newOrderNo;
//		$condition['prefix'] = '';
//		$this->db(1, CIRDB_CONFIG)->where($condition)->save($data);
		
		$sql = 'UPDATE cirdb_orderno SET idx = ' . $newOrderNo;
		$this->db(1, CIRDB_CONFIG)->execute($sql);
		
		SyncLogs::writeLog('N', 'update OrderNo to Cir: ' . $newOrderNo, __FUNCTION__);
	}
}

?>