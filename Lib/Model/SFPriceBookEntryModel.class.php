<?php

class SFPriceBookEntryModel extends Model {

	// get one or a list of PriceBookEntry
	public function getPriceBookEntry($sfConnection, &$ids) {	
		// query
		$query = "SELECT Id, Name, ProductCode from PriceBookEntry limit 10";
		$response = $sfConnection->query($query);
		
		foreach ($response->records as $record) {
			// echo $record->Id . ": " . $record->Name . " " . $record->ProductCode . PHP_EOL;
			array_push($ids, $record->Id);
		}
	}
	
	// get PriceBookEntry by Product ID
	public function getPriceBookEntryByProductId($sfConnection, $ProductId) {
		$query = "SELECT Id, Name, Pricebook2Id, Product2Id, ProductCode, SystemModstamp, UnitPrice, UseStandardPrice 
					FROM PricebookEntry WHERE Product2Id = '" . $ProductId . "'";
		
		$response = $sfConnection->query($query);
		
		return $response;
	}
	
	// upsert one PriceBookEntry
	public function upsertPriceBookEntry($sfConnection, $d, $i, $Product2Id) {
		$sObject = new stdClass();
		
		// get Standard Price Book Id
		$PriceBook2 = D('SFPriceBook2');
		$StdPriceBook = $PriceBook2->getStandardPriceBook($sfConnection);
		
		$sObject->IsActive = true;
		// $sObject->Name = $d[$i]["desc"];
		$sObject->Pricebook2id = $StdPriceBook->records[0]->Id;
		$sObject->Product2id = $Product2Id;
		// $sObject->ProductCode = $d[$i]["code"];
		$sObject->UnitPrice = $d[$i]["hk_price"];
		$sObject->UseStandardPrice = false;
		
		 $respone = $sfConnection->upsert('Id', array($sObject), 'PriceBookEntry');		// dev - ext id problem
		// $respone = $sfConnection->create(array($sObject), 'PriceBookEntry');
	}
	
	// delete PriceBookEntry
	public function delPriceBookEntry($sfConnection, $ids) {		
		$response = $sfConnection->delete($ids);

		return $response.success;
	}
}

?>