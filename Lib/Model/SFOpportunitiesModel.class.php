<?php

class SFOpportunitiesModel extends Model {

	// get Opportunities by Id
	public function getOpportunitiesById($sfConnection, &$ids) {
		
		// query
		$query = "SELECT Id, Name, ProductCode from Opportunity where id = " . $ids;
		$response = $sfConnection->query($query);
		
		foreach ($response->records as $record) {
			// echo $record->Id . ": " . $record->Name . " " . $record->ProductCode . PHP_EOL;
			array_push($ids, $record->Id);
		}
	}
	
/*	
	// get Opportunities by Modify date (Mag)
	public function getOpportuniitesByModifyDate($sfConnection, $mDate) {
		// query
		$query = "SELECT 
Opportunity.Name, 
Opportunity.Account.AccountNumber,
Opportunity.Paycode__c,
Opportunity.Paydtl1__c,
Opportunity.Paydtl2__c,
Opportunity.Paydtl3__c,
Opportunity.Paydtl4__c,
Opportunity.Paydtl5__c,
Opportunity.CreatedDate,
Opportunity.IsPaid__c,
Opportunity.IsSent__c,
Opportunity.Mail_Date__c,
Opportunity.Mail_Code__c,
Opportunity.HSI__C,
Opportunity.Pickup_Shop_Code__r.place__c,
(SELECT 
OpportunityLineItem.Custom_Issue__c,
OpportunityLineItem.Issue_Start__c,
OpportunityLineItem.ListPrice,
OpportunityLineItem.PricebookEntry.ProductCode 
From OpportunityLineItems)
FROM Opportunity 
WHERE Opportunity.LastModifiedDate >" . $mDate;
		
		$response = $sfConnection->query($query);
		
		return $response;
	}
	*/
	
	// upsert Opportunities from Cirdb (Mag)
	public function upsertOpportunitiesFromCir($sfConnection, $d) {
		$sObject = new stdClass();
		
		$SFAccount = D('SFAccount');
		$SFPickupShop = D('SFPickupShop');
		$PriceBook2 = D('SFPriceBook2');
		for ($i=0; $i < count($d); $i++) {
			$AccountNumber = $d[$i]["cust_code"];
			$Account = $SFAccount->getAccountByAccountNumber($sfConnection, $AccountNumber);
			
			$sObject->AccountId = $Account->records[0]->Id;
			
			$sObject->cirdb_order_code__c = $d[$i]["order_code"];			// Ext ID
			$sObject->CloseDate = $d[$i]["entry_date"];
			if ($d[$i]["mail"] == 1) { $sObject->IsSent__c = 'Yes'; }
				else { $sObject->IsSent__c = 'No'; }
			if ($d[$i]["payment"] == 1) { $sObject->IsPaid__c = 'Yes'; }
				else { $sObject->IsSent__c = 'No'; }
			$sObject->Mail_Code__c = $d[$i]["mail_code"];
/*			if ( $d[$i]["mail_date"] <> '0000-00-00' ) 
				{ $sObject->Mail_Date__c = $d[$i]["mail_date"]; 
				  $sObject->fieldsToNull = array(""); }
				else { $sObject->fieldsToNull = array("Mail_Date__c"); } */
			$sObject->HSI__c = $d[$i]["hsi"];
			
			$sObject->Name = OPP_PREF_ED_SU . date('Y', strtotime($d[$i]["entry_date"])) . date('m', strtotime($d[$i]["entry_date"]));
			if ( $d[$i]["paycode"] == 'A' ) { $sObject->Paycode__c = 'Cheque'; }
				elseif ( $d[$i]["paycode"] == 'B' ) { $sObject->Paycode__c = 'Deposit'; }
				elseif ( $d[$i]["paycode"] == 'C' ) { $sObject->Paycode__c = 'Credit Card'; }
				elseif ( $d[$i]["paycode"] == 'D' ) { $sObject->Paycode__c = 'No Charge'; }
			
			$sObject->Paydtl1__c = $d[$i]["paydtl1"];
			$sObject->Paydtl2__c = $d[$i]["paydtl2"];
			$sObject->Paydtl3__c = $d[$i]["paydtl3"];
			$sObject->Paydtl4__c = $d[$i]["paydtl4"];
			$sObject->Paydtl5__c = $d[$i]["paydtl5"];
			
			$SFPickup = $SFPickupShop->getPickupShopByPlace($sfConnection, $d[$i]["place"]);
			$sObject->Pickup_Shop_Code__c = $SFPickup->records[0]->Id;
			
			$StdPriceBook = $PriceBook2->getStandardPriceBook($sfConnection);
			$sObject->Pricebook2Id = $StdPriceBook->records[0]->Id;
			
			$sObject->Probability = 100;
			$sObject->StageName = 'Closed Won';
			$sObject->Type = 'New Business';			// dev

			$respone = $sfConnection->upsert('cirdb_order_code__c', array($sObject), 'Opportunity');
			
			if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Opportunities(Cir): ' . $d[$i]["order_code"] , __FUNCTION__);}
				else { SyncLogs::writeLog('E', 'Opportunities(Cir): ' . $d[$i]["order_code"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
			
			// if success, update OpportunityLineItem
			if ( $respone[0]->success == true ) {
				$SFOpportunityLineItem = D('SFOpportunityLineItem');
				$SFOpportunityLineItem->upsertOpportunityLineItemCir($sfConnection, $d, $i, $respone[0]->id);
			}
		}
	}
	
	// update Opportunities from ED Subscription (Mag)
	public function upsertOpportunitiesFromEDSubscription($sfConnection, $d) {
		$sObject = new stdClass();
		
		$SFAccount = D('SFAccount');
		$EDPickupShop = D('EDPickupShop');
		$SFPickupShop = D('SFPickupShop');
		$PriceBook2 = D('SFPriceBook2');
		for ($i=0; $i < count($d); $i++) {
			$SubscriptionId = $d[$i]["id"];		
			if ( $d[$i]["subscription_number"] <> '' ) { $Account = $SFAccount->getAccountByAccountNumber($sfConnection, $d[$i]["subscription_number"]); }
				else { $Account = $SFAccount->getAccountBySubscriptionId($sfConnection, $SubscriptionId); }		
			$sObject->AccountId = $Account->records[0]->Id;
			
			$sObject->ed_subscription2011_id__c = $d[$i]["id"];			// Ext ID
			$sObject->CloseDate = $d[$i]["addtime"];
			if ( $d[$i]["is_label_sent"] == 1) { $sObject->IsSent__c = 'Yes'; }
				else { $sObject->IsSent__c = 'No'; }
			if ( $d[$i]["is_paid"] == 1) { $sObject->IsPaid__c = 'Yes'; }
				else { $sObject->IsPaid__c = 'No'; }
			$sObject->Name = OPP_PREF_ED_SU . date('Y', strtotime($d[$i]["addtime"])) . date('m', strtotime($d[$i]["addtime"])) ;
			if ( $d[$i]["payment_type"] == '1' ) { $sObject->Paycode__c = 'Cheque'; }
				elseif ( $d[$i]["payment_type"] == '2' ) { $sObject->Paycode__c = 'Credit Card'; }

			$sObject->Paydtl1__c = $d[$i]["bank"];
			$sObject->Paydtl2__c = $d[$i]["cheque_no"];

			$EDPickupCode = $EDPickupShop->getPickupShopById($d[$i]["pickup_shop"]);			
			$SFPickup = $SFPickupShop->getPickupShopByCode($sfConnection, $EDPickupCode[0][code]);
			$sObject->Pickup_Shop_Code__c = $SFPickup->records[0]->Id;
				
			$StdPriceBook = $PriceBook2->getStandardPriceBook($sfConnection);
			$sObject->Pricebook2Id = $StdPriceBook->records[0]->Id;
				
			$sObject->Apply_Channel__c = 'Web Form';
			$sObject->Probability = 10;
			$sObject->StageName = 'Prospecting';			// for data import from ED CMS, user need to double check b4 set Closed Won
			if ( $d[$i]["Type"] == 1) { $sObject->Type = 'New Business'; }
				else { $sObject->Type = 'Existing Business'; }
				
			$respone = $sfConnection->upsert('ed_subscription2011_id__c', array($sObject), 'Opportunity');

			if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Opportunities(Mag): ' . $respone[0]->id . ', ' . $d[$i]["id"] , __FUNCTION__);}
			else { SyncLogs::writeLog('E', 'Opportunities(Mag): ' . $d[$i]["id"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
				
			// if success, update OpportunityLineItem
			if ( $respone[0]->success == true ) {
				$SFOpportunityLineItem = D('SFOpportunityLineItem');
				$SFOpportunityLineItem->upsertOpportunityLineItemSubscription($sfConnection, $d, $i, $respone[0]->id);
			}
		}
	}
	
	// update Opportunities from ED Store Invoice (eMag)
	public function upsertOpportunitiesFromEDStoreInvoice($sfConnection, $d) {
		$sObject = new stdClass();
		
		$SFAccount = D('SFAccount');
		$PriceBook2 = D('SFPriceBook2');
		for ($i=0; $i < count($d); $i++) {			
			$mid = $d[$i]["mid"];
			$Account = $SFAccount->getAccountByMemberEDId($sfConnection, $mid);
			$sObject->AccountId = $Account->records[0]->Id;
				
			$sObject->ed_store_invoice_detail_id__c = $d[$i]["did"];			// Ext ID
			$sObject->CloseDate = date('Y-m-d\TH:i:s\+08:00', $d[$i]["payment_date"]);
			$sObject->Name = OPP_PREF_ED_ES . date('Y', $d[$i]["payment_date"]) . date('m', $d[$i]["payment_date"]) ;
			
			//if ( $d[$i]["payment_type"] = '1' ) { $sObject->Paycode__c = 'Cheque'; }
			//	elseif ( $d[$i]["payment_type"] = '2' ) { $sObject->Paycode__c = 'Credit Card'; }
				
			$StdPriceBook = $PriceBook2->getStandardPriceBook($sfConnection);
			$sObject->Pricebook2Id = $StdPriceBook->records[0]->Id;
				
			$sObject->Apply_Channel__c = 'App';
			$sObject->Probability = 100;
			$sObject->StageName = 'Closed Won';
			if ( $d[$i]["Type"] == 1) { $sObject->Type = 'New Business'; }
				else { $sObject->Type = 'Existing Business'; }
				
			$respone = $sfConnection->upsert('ed_store_invoice_detail_id__c', array($sObject), 'Opportunity');

			if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Opportunities(eMag): ' . $respone[0]->id . ', ' . $d[$i]["did"] , __FUNCTION__);}
				else { SyncLogs::writeLog('E', 'Opportunities(eMag): ' . $d[$i]["did"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
			
			// if success, update OpportunityLineItem
			if ( $respone[0]->success == true ) {
				$SFOpportunityLineItem = D('SFOpportunityLineItem');
				$SFOpportunityLineItem->upsertOpportunityLineItemStoreInvoice($sfConnection, $d, $i, $respone[0]->id);
			}
		}
	}
	
	// update Opportunities from ED Promotion
	public function upsertOpportunitiesFromEDPromotion($sfConnection, $d) {
		$sObject = new stdClass();
		
		$SFAccount = D('SFAccount');
		$PromoCode = D('SFPromoCode');
		$EDPromoCode = D('EDPromoCode');
		//$PriceBook2 = D('SFPriceBook2');
		
		for ($i=0; $i < count($d); $i++) {
			$mid = $d[$i]["mid"];
			$Account = $SFAccount->getAccountByMemberEDId($sfConnection, $mid);
			$sObject->AccountId = $Account->records[0]->Id;
		
			$sObject->cirdb_order_code__c = 'p' . $d[$i]["id"];				// Ext ID, add 'P' as prefix
			$sObject->CloseDate = date('Y-m-d\TH:i:s\+08:00', $d[$i]["addtime"]);
			$sObject->Name = OPP_PREF_ED_EP . date('Y', $d[$i]["addtime"]) . date('m', $d[$i]["addtime"]) ;
			$sObject->Paycode__c = 'No Charge';
			$sObject->Description = $d[$i]["download_code_client_remark"];
			$Code = $PromoCode->getPromoCodeByCode($sfConnection, $d[$i]["promotioncode"]);
			$sObject->Promotion_Code__c = $Code;
			$sObject->Promotion_Batch_Name__c = $d[$i]["batch_name"];
			$sObject->Promotion_Client_Type__c = $d[$i]["client_type_name"];
		
			//$StdPriceBook = $PriceBook2->getStandardPriceBook($sfConnection);
			//$sObject->Pricebook2Id = $StdPriceBook->records[0]->Id;
		
			$sObject->Apply_Channel__c = 'App';
			if( $d[$i]["client_type_code"] == 'Cir3' || $d[$i]["client_type_code"] == 'Gmkt1') { 
				$sObject->Probability = 10;
				$sObject->StageName = 'Prospecting'; }
			else {
				$sObject->Probability = 100;
				$sObject->StageName = 'Closed Won'; }

			$respone = $sfConnection->upsert('cirdb_order_code__c', array($sObject), 'Opportunity');
			
			if ( $respone[0]->success == true ) { 
				SyncLogs::writeLog('N', 'upsert Opportunities(Promo): ' . $respone[0]->id . ', ' . $d[$i]["id"] , __FUNCTION__);
				
				// update OpportunityLineItem
				$SFOpportunityLineItem = D('SFOpportunityLineItem');
				$SFOpportunityLineItem->upsertOpportunityLineItemEDPromotion($sfConnection, $d, $i, $respone[0]->id);
				
				// if success, update promotion code master
				$EDCode = $EDPromoCode->getPromoCodeByCode($d[$i]["promotioncode"]);
				$result = $PromoCode->upsertPromoCode($sfConnection, $EDCode);

			}
				else { SyncLogs::writeLog('E', 'Opportunities(Promo): ' . $d[$i]["id"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
		}
	}
}

?>