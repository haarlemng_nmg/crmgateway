<?php

class SFOpportunityLineItemModel extends Model {

	// get OpportunityLineItem By Modify Date
	public function getOpportunityLineItemByModifyDate($sfConnection, $mDate) {
		$query = "SELECT
			OpportunityLineItem.Id,
			OpportunityLineItem.Opportunity.Id,
			OpportunityLineItem.Opportunity.Name,
			OpportunityLineItem.Opportunity.Paycode__c,
			OpportunityLineItem.Opportunity.Paydtl1__c,
			OpportunityLineItem.Opportunity.Paydtl2__c,
			OpportunityLineItem.Opportunity.Paydtl3__c,
			OpportunityLineItem.Opportunity.Paydtl4__c,
			OpportunityLineItem.Opportunity.Paydtl5__c,
			OpportunityLineItem.Opportunity.CreatedDate,
			OpportunityLineItem.Opportunity.IsPaid__c,
			OpportunityLineItem.Opportunity.IsSent__c,
			OpportunityLineItem.Opportunity.Mail_Date__c,
			OpportunityLineItem.Opportunity.Mail_Code__c,
			OpportunityLineItem.Opportunity.HSI__C,
			OpportunityLineItem.Opportunity.cirdb_order_code__c,
			OpportunityLineItem.Opportunity.Pickup_Shop_Code__r.place__c,
			OpportunityLineItem.Opportunity.Account.AccountNumber,
			OpportunityLineItem.Custom_Issue__c,
			OpportunityLineItem.Issue_Start__c,
			OpportunityLineItem.ListPrice,
			OpportunityLineItem.PricebookEntry.ProductCode,
			OpportunityLineItem.PricebookEntry.Product2.Family
			From OpportunityLineItem
			WHERE OpportunityLineItem.IsDeleted = false
			AND OpportunityLineItem.Opportunity.IsDeleted = false
			AND OpportunityLineItem.Opportunity.StageName = 'Closed Won'
			AND OpportunityLineItem.PricebookEntry.Product2.Family = 'EDWM'
			AND OpportunityLineItem.Opportunity.LastModifiedDate > " . $mDate;
		
		$response = $sfConnection->query($query);
	
		return $response;
	}
	
	// update OpportunityLineItem from Cirdb
	public function upsertOpportunityLineItemCir($sfConnection, $d, $i, $OpportunityId) {
		$sObject = new stdClass();
		
		$sObject->cirdb_order_code__c = $d[$i]["order_code"];
		$sObject->Custom_Issue__c = $d[$i]["custom_issue"];
		$sObject->Issue_Start__c = $d[$i]["issuestart"];
		// $sObject->ListPrice = $d[$i]["custom_price"];
		$sObject->Opportunityid = $OpportunityId;				// compulsory
		
		// product -> price2
		$SFProduct = D('SFProduct');
		$Product = $SFProduct->getProductByProdCode($sfConnection, $d[$i]["pkgcode"]);
		$SFPriceBookEntry = D('SFPriceBookEntry');
		$PriceBookEntry = $SFPriceBookEntry->getPriceBookEntryByProductId($sfConnection, $Product->records[0]->Id);
		$sObject->PricebookEntryid = $PriceBookEntry->records[0]->Id;
		
		$sObject->Quantity = 1;
		$sObject->ServiceDate = $d[$i]["entry_date"];
		// $sObject->TotalPrice = $d[$i]["custom_price"];
		$sObject->UnitPrice = $d[$i]["custom_price"];

		$OpportunityLineItem = $sfConnection->upsert('cirdb_order_code__c', array($sObject), 'OpportunityLineItem');
		
		if ( $OpportunityLineItem[0]->success == true ) { SyncLogs::writeLog('N', 'upsert OpportunityLine(Cir): ' . $d[$i]["order_code"] , __FUNCTION__);}
		else { SyncLogs::writeLog('E', 'OpportunityLine(Cir): ' . $d[$i]["order_code"] . ', Error: "' . $OpportunityLineItem[0]->errors[0]->message . '"', __FUNCTION__); }
	}
	
	// update OpportunityLineItem from ED Subscription
	public function upsertOpportunityLineItemSubscription($sfConnection, $d, $i, $OpportunityId) {
		$sObject = new stdClass();
		
		$EDSubscription = D('EDSubscription');
		$GrossAmt = $EDSubscription->getEDSubscriptionGrossAmount($d[$i]["id"]);
		// $sObject->ListPrice = $GrossAmt;
		
		$sObject->Opportunityid = $OpportunityId;				// compulsory
		$sObject->ed_subscription2011_id__c = $d[$i]["id"];
		
		$p = D('SFProduct');
		$product = $p->getProductByPlanId($sfConnection, $d[$i]["plan"]);
		$pbe = D('SFPriceBookEntry');
		$pbe_id = $pbe->getPriceBookEntryByProductId($sfConnection, $product->records[0]->Id);		
		$sObject->PricebookEntryId = $pbe_id->records[0]->Id;
		
		$sObject->Quantity = 1;
		$sObject->ServiceDate = $d[$i]["addtime"];
		// $sObject->TotalPrice = $GrossAmt;
		if ( $GrossAmt == '' ) { $sObject->UnitPrice = $pbe_id->records[0]->UnitPrice; }
			else { $sObject->UnitPrice = $GrossAmt; }
		
		$OpportunityLineItem = $sfConnection->upsert('ed_subscription2011_id__c', array($sObject), 'OpportunityLineItem');
		
		if ( $OpportunityLineItem[0]->success == true ) { SyncLogs::writeLog('N', 'upsert OpportunityLine(Mag): ' . $OpportunityId . ', ' . $d[$i]["id"] , __FUNCTION__);}
		else { SyncLogs::writeLog('E', 'OpportunityLine(Mag): ' . $OpportunityId . ', ' . $d[$i]["id"] . ', Error: "' . $OpportunityLineItem[0]->errors[0]->message . '"', __FUNCTION__); }
	}
	
	// update OpportunityLineItem from ED Store Invoice
	public function upsertOpportunityLineItemStoreInvoice($sfConnection, $d, $i, $OpportunityId) {
		$sObject = new stdClass();
		
		$sObject->Opportunityid = $OpportunityId;				// compulsory
		$sObject->ed_store_invoice_detail_id__c = $d[$i]["did"];
		
		if( $d[$i]["pid"] == 1 ) { $ProdCode = 'EDEMA58'; }
			elseif ( $d[$i]["pid"] == 2 ) { $ProdCode = 'EDEMA158'; }
			elseif ( $d[$i]["pid"] == 3 ) { $ProdCode = 'EDEMA208'; }
			elseif ( $d[$i]["pid"] == 4 ) { $ProdCode = 'EDEMA308'; }
			else { $ProdCode = 'EDEMA23'; }
		$p = D('SFProduct');
		$product = $p->getProductByProdCode($sfConnection, $ProdCode);
		
		$pbe = D('SFPriceBookEntry');
		$pbe_id = $pbe->getPriceBookEntryByProductId($sfConnection, $product->records[0]->Id);
		$sObject->PricebookEntryId = $pbe_id->records[0]->Id;
	
		$sObject->Quantity = 1;
		$sObject->ServiceDate = date('Y-m-d\TH:i:s\+08:00', $d[$i]["updatetime"]);
		// $sObject->TotalPrice = $d[$i]["payment_amt"];
		$sObject->UnitPrice = $d[$i]["payment_amt"];
		if ( $d[$i][pid] <> 1 AND $d[$i][pid] <> 2 AND $d[$i][pid] <> 3 AND $d[$i][pid] <> 4) 
			{ $sObject->Issue_Start__c = $d[$i]["name"]; }
		
		$OpportunityLineItem = $sfConnection->upsert('ed_store_invoice_detail_id__c', array($sObject), 'OpportunityLineItem');
		
		if ( $OpportunityLineItem[0]->success == true ) { SyncLogs::writeLog('N', 'upsert OpportunityLine(eMag): ' . $d[$i]["did"] , __FUNCTION__);}
		else { SyncLogs::writeLog('E', 'OpportunityLine(eMag): ' . $d[$i]["did"] . ', Error: "' . $OpportunityLineItem[0]->errors[0]->message . '"', __FUNCTION__); }
	}
	
	// update OpportunityLineItem from ED promotion
	public function upsertOpportunityLineItemEDPromotion($sfConnection, $d, $i, $OpportunityId) {
		$sObject = new stdClass();
		
		$sObject->Opportunityid = $OpportunityId;				// compulsory
		$sObject->cirdb_order_code__c = 'p' . $d[$i]["id"];

		$p = D('SFProduct');		
		$product = $p->getProductByProdCode($sfConnection, 'EDEMAPROM');
		
		$pbe = D('SFPriceBookEntry');
		$pbe_id = $pbe->getPriceBookEntryByProductId($sfConnection, $product->records[0]->Id);
		$sObject->PricebookEntryId = $pbe_id->records[0]->Id;
		
		$sObject->Quantity = 1;
		$sObject->ServiceDate = date('Y-m-d\TH:i:s\+08:00', $d[$i]["addtime"]);
		$sObject->Custom_Issue__c = (int)$d[$i]["n_coming_issue"];
		$sObject->Issue_Start__c = (int)$d[$i]["start_issue"];
		$sObject->UnitPrice = 0;

		$OpportunityLineItem = $sfConnection->upsert('cirdb_order_code__c', array($sObject), 'OpportunityLineItem');
		
		if ( $OpportunityLineItem[0]->success == true ) { SyncLogs::writeLog('N', 'upsert OpportunityLine(Promo): ' . $OpportunityId . ', ' . $d[$i]["id"] , __FUNCTION__);}
		else { SyncLogs::writeLog('E', 'OpportunityLine(Promo): ' . $OpportunityId . ', ' . $d[$i]["id"] . ', Error: "' . $OpportunityLineItem[0]->errors[0]->message . '"', __FUNCTION__); }
	}
	
}

?>