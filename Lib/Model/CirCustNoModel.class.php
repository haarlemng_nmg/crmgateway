<?php

class CirCustNoModel extends Model {
	protected $trueTableName = 'cirdb_custno';
	
	public function getCustomerCode() {
		C('DB_CHARSET','utf8');
		
		// get next customer no from Cirdb
		$cust_code = $this->db(1, CIRDB_CONFIG)->select();
		
		return $cust_code;
	}
	
	public function updateCustNoFromSF($sfConnection) {
		C('DB_CHARSET','utf8');
		
		// get cust_code from SF
		$SFGateway = D('SFCRMGateway');
		$new = $SFGateway->getCustNo($sfConnection, 'ED');
		
		// update new customer no to Cirdb
		$data['local_count'] = $new;
		$condition['prefix'] = 'ED';	
		$this->db(1, CIRDB_CONFIG)->where($condition)->save($data);

//		$query = 'UPDATE cirdb_custno SET local_count = ' . $new;
//		$this->execute($query);
		
		SyncLogs::writeLog('N', 'update CustNo to Cirdb: ' . $new, __FUNCTION__);
	}
}

?>