<?php

class CirCustomerModel extends Model {
	protected $trueTableName = 'cirdb_customers';

	// -- get Customer records --
	public function getCustomer() {
		C('DB_CHARSET','utf8');

		// query, one time import
		$condition['cirdb_customers.active'] = 1;
		$condition['substring(cirdb_customers.cust_code,3,6)'] = array(array('EGT', 17748), array('ELT', 17751));
		
		$d = $this->db(1, CIRDB_CONFIG)
				->join('cirdb_customersdetails on cirdb_customers.cust_code = cirdb_customersdetails.cust_code')
				->field('cirdb_customers.*, cirdb_customersdetails.*')
				->where($condition)->select();

		return $d;
	}
	
	public function checkAccountNumber($accountNumber) {
		C('DB_CHARSET','utf8');
		
		$condition['cust_code'] = $accountNumber;
		
		$d = $this->db(1, CIRDB_CONFIG)
				->field('count(cust_code) ctn')
				->where($condition)->select();
		
		return $d;
	}
	
	// -- insert Customer records from Personal Account --
	public function insertCustomerFromPersonalAccount($d) {
		C('DB_CHARSET','utf8');		
		
		// update customer
		$data = D('CirCustomer');
		
		for ($i=0; $i < $d->size; $i++) {
			$data->cust_code = $d->records[$i]->AccountNumber;
			$data->remark = $d->records[$i]->Description . '';
			$data->fax = $d->records[$i]->Fax . '';
			$data->contact = $d->records[$i]->FirstName . ' ' . $d->records[$i]->LastName . '';
			if ( $d->records[$i]->Location__c == '香港') { $data->location = 1; }
				elseif ( $d->records[$i]->Location__c == '亞洲') { $data->location = 2; }
				elseif ( $d->records[$i]->Location__c == '中國') { $data->location = 3; }
				else { $data->location = 4; }
			$data->telnext2 = $d->records[$i]->PersonMobilePhone . '';
			$data->greet = $d->records[$i]->Salutation . '';
			$data->telnext1 = $d->records[$i]->Phone . '';
			
			$addr = explode('
', $d->records[$i]->ShippingStreet);
			$data->add1 = $addr[0] . '';
			$data->add2 = $addr[1] . '';
			$data->add3 = $addr[2] . '';
			$last_addr = $addr[3];
			for ($j = 4; $j<=count($addr); $j++) {
				$last_addr = $last_addr .' '. $addr[$j];
			}
			$data->add4 = $last_addr . '';
			
			$data->date = date('Y-m-d', strtotime($d->records[$i]->CreatedDate));
			$data->active = 1;			

			$count = $this->checkAccountNumber($d->records[$i]->AccountNumber);		
			if ( $count[0][ctn] == 0 ) { 
				$result = $data->db(1, CIRDB_CONFIG)->add(); 
				
				if ( $result !== false ) { SyncLogs::writeLog('N', 'add Customer to Cir - personal: ' . $d->records[$i]->AccountNumber, __FUNCTION__); } 
					else { SyncLogs::writeLog('E', 'add Customer to Cir - personal: ' . $d->records[$i]->AccountNumber . ', Query: ' . $data->getLastSql() , __FUNCTION__); }
			}
			else { 
				$result = $data->db(1, CIRDB_CONFIG)->save();
				
				if ( $result !== false ) { SyncLogs::writeLog('N', 'update Customer to Cir - personal: ' . $d->records[$i]->AccountNumber, __FUNCTION__); } 
					else { SyncLogs::writeLog('E', 'update Customer to Cir - personal: ' . $d->records[$i]->AccountNumber . ', Query: ' . $data->getLastSql() , __FUNCTION__); }
			}
			
			if ( $result !== false ) {
				// update customer details
				$CirCustomerDetails = D('CirCustomerDetails');
				$CirCustomerDetails->insertCustomerDetails($d, $i, true);
			}
		}		
	}
	
	// -- insert Customer records from Business Account --
	public function insertCustomerFromBusinessAccount($d) {
		C('DB_CHARSET','utf8');
	
		// update customer
		$data = D('CirCustomer');

		for ($i=0; $i < $d->size; $i++) {
			$data->cust_code = $d->records[$i]->Account->AccountNumber;
			$data->remark = $d->records[$i]->Account->Description . '';
			$data->fax = $d->records[$i]->Account->Fax . '';
			$data->contact = $d->records[$i]->FirstName . ' ' . $d->records[$i]->LastName . '';
			$data->co = $d->records[$i]->Account->Name . '';
			if ( $d->records[$i]->Account->Location__c == '香港') { $data->location = 1; }
				elseif ( $d->records[$i]->Account->Location__c == '亞洲') { $data->location = 2; }
				elseif ( $d->records[$i]->Account->Location__c == '中國') { $data->location = 3; }
				else { $data->location = 4; }
			$data->greet = $d->records[$i]->Salutation . '';
			$data->telnext1 = $d->records[$i]->MobilePhone . '';
			$data->telnext2 = $d->records[$i]->Phone . '';
		
			$addr = explode('
', $d->records[$i]->Account->ShippingStreet);
			$data->add1 = $addr[0] . '';
			$data->add2 = $addr[1] . '';
			$data->add3 = $addr[2] . '';
			$last_addr = $addr[3];
			for ($j = 4; $j<=count($addr); $j++) {
				$last_addr = $last_addr .' '. $addr[$j];
			}
			$data->add4 = $last_addr . '';
			
			$data->date = date('Y-m-d', strtotime($d->records[$i]->Account->CreatedDate));
			$data->active = 1;

			$count = $this->checkAccountNumber($d->records[$i]->Account->AccountNumber);
			if ( $count[0][ctn] == 0 ) { 
				$result = $data->db(1, CIRDB_CONFIG)->add(); 
				
				if ( $result !== false ) { SyncLogs::writeLog('N', 'add Customer to Cir - business: ' . $d->records[$i]->Account->AccountNumber, __FUNCTION__); }
					else { SyncLogs::writeLog('E', 'add Customer to Cir - business: ' . $d->records[$i]->Account->AccountNumber . ', Query: ' . $data->getLastSql() , __FUNCTION__); }
			}
			else { 
				$result = $data->db(1, CIRDB_CONFIG)->save(); 
				
				if ( $result !== false ) { SyncLogs::writeLog('N', 'update Customer to Cir - business: ' . $d->records[$i]->Account->AccountNumber, __FUNCTION__); }
					else { SyncLogs::writeLog('E', 'update Customer to Cir - business: ' . $d->records[$i]->Account->AccountNumber . ', Query: ' . $data->getLastSql() , __FUNCTION__); }
			}	
				
			if ( $result !== false ) {
				// update customer details
				$CirCustomerDetails = D('CirCustomerDetails');
				$CirCustomerDetails->insertCustomerDetails($d, false);
			}
		}
	}
	
}

?>