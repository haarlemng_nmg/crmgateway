<?php

class SFPriceBook2Model extends Model {

	// get Standard Price Book
	public function getStandardPriceBook($sfConnection) {		
		// query
		$query = "SELECT id, isActive, isStandard, Name from PriceBook2";
		$response = $sfConnection->query($query); 
		
		if ($response->size == 1) {
			return $response;
		}
	}
}

?>