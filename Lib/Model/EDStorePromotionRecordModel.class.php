<?php

class EDStorePromotionRecordModel extends Model {
	// actual table name on ED CMS
	protected $trueTableName = 'store_promotioncode_record';

	// get ED Promotion record
	public function getEDStorePromotionRecord($cutOff) {
		C('DB_CHARSET','latin1');
		
		// query
		$condition['store_promotioncode_record.addtime'] = array('EGT', $cutOff);
		
		$d = $this->db(0)
				->join('ipad2011_download_code_batch on ipad2011_download_code_batch.id = store_promotioncode_record.download_code_batch')
				->join('ipad2011_download_code_client_type on ipad2011_download_code_client_type.id = store_promotioncode_record.download_code_client_type')
				->field('store_promotioncode_record.*, ipad2011_download_code_batch.n_coming_issue, ipad2011_download_code_batch.cname batch_name, ipad2011_download_code_client_type.cname client_type_name, ipad2011_download_code_client_type.name client_type_code')
				->where($condition)->select();
		
		return $d;
	}
}

?>