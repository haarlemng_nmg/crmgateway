<?php

class EDStoreInvoiceModel extends Model {
	// actual table name on ED CMS
	protected $trueTableName = 'store_invoice';

	// get ED Subscription
	public function getEDStoreInvoice($cutOff) {
		C('DB_CHARSET','latin1');
		
		// query
		$condition['store_invoice_detail.payment_date'] = array('EGT', $cutOff);
		$condition['store_invoice_detail.payment_status'] = array('IN', 'Completed,RecurringPaymentCreated,RecurringPaymentRenew');
		
		$d = $this->db(0)
				->join('store_invoice_detail on store_invoice.id = store_invoice_detail.invoice_id')
				->join('store_invoice_product on store_invoice.id = store_invoice_product.invoice_id')
				->join('store_product on store_invoice_product.product_id = store_product.id')
				->field('store_invoice.id, store_invoice.mid,store_invoice_detail.id did,store_invoice.updatetime, store_invoice_detail.payment_status, store_invoice_detail.payment_amt, store_invoice_detail.payment_date, store_product.name, store_product.id pid')
				->where($condition)->select();
		
		return $d;
	}
}

?>