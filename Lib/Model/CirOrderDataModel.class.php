<?php

class CirOrderDataModel extends Model {
	protected $trueTableName = 'cirdb_orderdata';

	public function getOrderData() {
		C('DB_CHARSET','utf8');

		// query, one time import
		$condition['active'] = 1;
		$condition['entry_date'] = array('NEQ', '0000-00-00');
		$condition['order_code'] = array('BETWEEN','038900,038999');		// dev
//		$condition['order_code'] = array('BETWEEN','042922,042926');		// dev
		
		$d = $this->db(1, CIRDB_CONFIG)->where($condition)->order('order_code desc')->select();
		
		//echo $this->getLastSql() . PHP_EOL;
			
		return $d;
	}
	
	public function insertOrderData($sfConnection, $d) {
		C('DB_CHARSET','utf8');
		
		$data = D('CirOrderData');
		$opp = D('SFOpportunities');
		$oppLine = D('SFOpportunityLineItem');
		$CirOrderNo = D('CirOrderNo');
		$orderNo = $CirOrderNo->getOrderNo();
		
		for ($i=0; $i < $d->size; $i++) {
			$sObject = new stdClass();
			
			if ( $d->records[$i]->Opportunity->cirdb_order_code__c == '' ) 
					{ $targetOrderCode = str_pad($orderNo, 6, '0', STR_PAD_LEFT);
					  $data->t_option = 'N';
					  $data->printcnt = 0;
					  $data->gift = 'NIL'; }
				else { $targetOrderCode = $d->records[$i]->Opportunity->cirdb_order_code__c; }
			$data->order_code = $targetOrderCode;
			$data->cust_code = $d->records[$i]->Opportunity->Account->AccountNumber;
			$data->issuestart = $d->records[$i]->Issue_Start__c . '';
			$data->pkgcode = $d->records[$i]->PricebookEntry->ProductCode;
			$data->custom_issue = $d->records[$i]->Custom_Issue__c . '';
			$data->custom_price = $d->records[$i]->ListPrice . '';
			if ( $d->records[$i]->Opportunity->Paycode__c == 'Cheque' ) { $data->paycode = 'A'; }
				elseif ( $d->records[$i]->Opportunity->Paycode__c == 'Bank' ) { $data->paycode = 'B'; }
				elseif ( $d->records[$i]->Opportunity->Paycode__c == 'Credit Card' ) { $data->paycode = 'C'; }
				elseif ( $d->records[$i]->Opportunity->Paycode__c == 'No Charge' ) { $data->paycode = 'D'; }
			$data->paydtl1 = $d->records[$i]->Opportunity->Paydtl1__c . '';
			$data->paydtl2 = $d->records[$i]->Opportunity->Paydtl2__c . '';
			$data->paydtl3 = $d->records[$i]->Opportunity->Paydtl3__c . '';
			$data->paydtl4 = $d->records[$i]->Opportunity->Paydtl4__c . '';
			$data->paydtl5 = $d->records[$i]->Opportunity->Paydtl5__c . '';
			$data->place = $d->records[$i]->Opportunity->Pickup_Shop_Code__r->place__c . '';
			$data->entry_date = date('Y-m-d H:i:s', strtotime($d->records[$i]->Opportunity->CreatedDate));
			$data->active = 1;
			If ( $d->records[$i]->Opportunity->IsPaid__c == 'Yes' ) { $data->payment = 1; }
				else { $data->payment = 0; }
			If ( $d->records[$i]->Opportunity->IsSent__c == 'Yes' ) { $data->mail = 1; }
				else { $data->mail = 0; }
			$data->mail_date = $d->records[$i]->Opportunity->Mail_Date__c . '';
			$data->mail_code = $d->records[$i]->Opportunity->Mail_Code__c . '';
			$data->hsi = $d->records[$i]->Opportunity->HSI__c . '';
			
			if ( $d->records[$i]->Opportunity->cirdb_order_code__c == '' ) {
				$result = $data->db(1, CIRDB_CONFIG)->add();
				
				if ( $result !== false ) { 
					SyncLogs::writeLog('N', 'add Order to Cirdb: ' . $targetOrderCode . ' Account: ' . $d->records[$i]->Opportunity->Account->AccountNumber , __FUNCTION__); 
				
					// update order_code to SF			
					$sObject->Id = $d->records[$i]->Opportunity->Id;
					$sObject->cirdb_order_code__c = $targetOrderCode;
					$r = $sfConnection->update(array($sObject), 'Opportunity');
					
					if ( $r[0]->success == true ) { SyncLogs::writeLog('N', 'update Opportunity Id: ' . $d->records[$i]->Opportunity->Id . ' OrderCode: ' . $targetOrderCode , __FUNCTION__); }
						else { SyncLogs::writeLog('E', 'update Opportunity Id: ' . $d->records[$i]->Opportunity->Id . ' OrderCode: ' . $targetOrderCode  . ', Error: "' . $r[0]->errors[0]->message . '"', __FUNCTION__); }
					
					$orderNo = $orderNo + 1;
				} else {
					SyncLogs::writeLog('E', 'add Order to Cirdb: ' . $targetOrderCode . ' Opportunity Id: ' . $d->records[$i]->Opportunity->Id . ', Query: ' . $data->getLastSql() , __FUNCTION__);
				}
			}
			else {				
				$condition['order_code'] = $targetOrderCode;			
				$result = $data->db(1, CIRDB_CONFIG)->where($condition)->save();

				if ( $result !== false ) { SyncLogs::writeLog('N', 'update Order to Cirdb: ' . $targetOrderCode . ' Account: ' . $d->records[$i]->Opportunity->Account->AccountNumber, __FUNCTION__); }	
					else { SyncLogs::writeLog('E', 'update Order to Cirdb: ' . $targetOrderCode . ' Opportunity Id: ' . $d->records[$i]->Opportunity->Id . ', Query: ' . $data->getLastSql() , __FUNCTION__); }		 
			}
		}
		
		// update orderno table
		$CirOrderNo->updateOrderNo($orderNo);

		return $orderNo;
	}
}

?>