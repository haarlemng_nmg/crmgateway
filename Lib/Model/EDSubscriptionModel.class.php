<?php

class EDSubscriptionModel extends Model {
	// actual table name on ED CMS
	protected $trueTableName = 'subscription2011';

	// get ED Subscription
	public function getEDSubscription($cutOff) {
		C('DB_CHARSET','latin1');
		
		// query
		$condition['subscription2011.status'] = 1;
		$condition['subscription2011.lastmodify'] = array('EGT', $cutOff);
		
		$d = $this->db(0)
				->join('subscription_plan_type on subscription2011.plan = subscription_plan_type.id')
				->field('subscription2011.*, subscription_plan_type.cname as planname')
				->where($condition)->select();
		
		return $d;
	}
	
	// get ED Subscription Gross Amount by Id
	public function getEDSubscriptionGrossAmount($id) {
		C('DB_CHARSET','latin1');
		
		// query
		$condition['subscription2011.id'] = $id;
		
		$d = $this->db(0)
				->join('payment_invoice on subscription2011.id = payment_invoice.subscription_id')
				->where($condition)
				->sum('mc_gross');
		
		return $d;	
	}
}

?>