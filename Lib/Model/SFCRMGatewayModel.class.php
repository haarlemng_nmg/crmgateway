<?php

class SFCRMGatewayModel extends Model {

	// get value from CRM Gateway object
	public function getCustNo($sfConnection, $bu) {
		$query = "SELECT Id, custno__c, Name FROM CRM_Gateway__c WHERE Name = '" . $bu . "'";
		$response = $sfConnection->query($query); 
		
		if ($response->size == 1) { return $response->records[0]->custno__c; }
	}
	
	public function getOrderNo($sfConnection, $bu) {
		$query = "SELECT Id, orderno__c, Name FROM CRM_Gateway__c WHERE Name = '" . $bu . "'";
		$response = $sfConnection->query($query);
		
		if ($response->size == 1) { return $response->records[0]->orderno__c; }
	}
	
	public function getUid($sfConnection, $bu) {
		$query = "SELECT Id, uid__c, Name FROM CRM_Gateway__c WHERE Name = '" . $bu . "'";
		$response = $sfConnection->query($query);
		
		if ($response->size == 1) { return $response->records[0]->uid__c; }
	}
	
	public function getGatewayId($sfConnection, $bu) {
		$query = "SELECT Id, Name FROM CRM_Gateway__c WHERE Name = '" . $bu . "'";
		$response = $sfConnection->query($query);
		
		if ($response->size == 1) { return $response->records[0]->Id; }
	}
	
	// update value to CRM Gateway object
	public function updateUid($sfConnection, $bu) {
		$sObject = new stdClass();
		
		$Account = D('SFAccount');
		$max = $Account->getMaxEDMember_id($sfConnection);
		
		$sObject->uid__c = $max;
		$sObject->Name = 'ED';
		
		$sfConnection->upsert('Name', array($sObject), 'CRM_Gateway__C');
		
		SyncLogs::writeLog('N', 'update CRMGateway uid: ' . $max, __FUNCTION__);
	}
	
	public function updateOrderNo($sfConnection, $orderNo) {
		$sObject = new stdClass();
		
		$sObject->orderno__c = $orderNo;
		$sObject->Name = 'ED';
		
		$sfConnection->upsert('Name', array($sObject), 'CRM_Gateway__C');
		
		SyncLogs::writeLog('N', 'update CRMGateway orderNo: ' . $orderNo, __FUNCTION__);
	}
}

?>