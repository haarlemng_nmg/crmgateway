<?php

class SFPromoCodeModel extends Model {

	// get one or a list of Promotion Code
	public function getPromoCode($sfConnection, &$ids) {
		// query
		$query = "SELECT Id, Name, Batch_ID__c, End_Date__c from Promotion_Code__c where End_Date__c < 2013-01-01 order by End_Date__c asc limit 200";
		$response = $sfConnection->query($query);
		
		foreach ($response->records as $record) {
			// echo $record->Id . ": " . $record->Name . " " . $record->Batch_ID__c . PHP_EOL;
			array_push($ids, $record->Id);
			
			//$r = $sfConnection->delete($record->Id);
			
			echo 'Id: ' . $record->Id . ' , ' . $record->End_Date__c . ' ' . $record->Name . ' ' . $r[0]->success . PHP_EOL;
		}
	}
	
	// get Promotion Code by Code
	public function getPromoCodeByCode($sfConnection, $code) {
		$query = "SELECT Id, Name FROM Promotion_Code__c WHERE Name = '" . $code . "'";
		$response = $sfConnection->query($query);
		
		return $response->records[0]->Id;
	}
	
	// upsert one or a list of Promotion Code
	public function upsertPromoCode($sfConnection, $d) {
		$sObject = new stdClass();
		
		for ($i=0; $i < count($d); $i++) {
			$sObject->Batch_ID__C = $d[$i]["oid"];
			$sObject->Coming_Issue__C = $d[$i]["n_coming_issue"];
			$sObject->End_Date__c = $d[$i]["enddate"];
			if ( $d[$i]["fix_issue"] <> null) { $sObject->Fix_Issue__c = $d[$i]["fix_issue"]; }
			$sObject->Name = $d[$i]["code"];
			$sObject->n_redemption__c = $d[$i]["n_redemption"];
			$sObject->Quota__c = $d[$i]["quota"];
			if ($d[$i]["status"] == 0) { $sObject->Status__c = 'Hidden'; }
				elseif ($d[$i]["status"] == 1) { $sObject->Status__c = 'Online'; }
				elseif ($d[$i]["status"] == 2) { $sObject->Status__c = 'Draft'; }
			if ($d[$i]["type"] == 1) { $sObject->Type__c = '動態延續訂閱'; }
				elseif ($d[$i]["type"] == 2) { $sObject->Type__c = '指定期數'; }
			if ($d[$i]["valid"] == 0) { $sObject->Valid__c = 'Invalid'; }
				elseif ($d[$i]["valid"] == 1) { $sObject->Valid__c = 'Valid'; }
		
			$respone = $sfConnection->upsert('Name', array($sObject), 'Promotion_Code__c');
			
			if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Promotion Code: ' . $d[$i]["code"] , __FUNCTION__);}
				else { SyncLogs::writeLog('E', 'upsert Promotion Code: ' . $d[$i]["code"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
		}
	}
	
	// delete one or a list of Promotion Code
	public function delPromoCode($sfConnection, $ids) {		
		$response = $sfConnection->delete($ids);

		return $response.success;
	}
}

?>