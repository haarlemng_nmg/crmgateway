<?php

class SFAccountModel extends Model {

	// get Account by Account Number
	public function getAccountByAccountNumber($sfConnection, $AccountNumber) {		
		$query = "SELECT Id, AccountNumber, LastName, Name from Account WHERE AccountNumber ='" . $AccountNumber . "'";
		$response = $sfConnection->query($query);
		
		return $response;

		/*		
		foreach ($response->records as $record) {
			// echo $record->Id . ": " . $record->Name . PHP_EOL;
			array_push($ids, $record->Id);
		} */
	}
	
	// get Account by Email
	public function getAccountByEmail($sfConnection, $email) {
		$query = "SELECT Id, AccountNumber, LastName, Name from Account WHERE Email ='" . $email . "'";
		$response = $sfConnection->query($query);
		
		return $response;
	}
	
	// get Account by Subscription ID
	public function getAccountBySubscriptionId($sfConnection, $SubscriptionId) {
		$query = "SELECT Id, AccountNumber, LastName, Name from Account WHERE ed_subscription2011_id__c = " . $SubscriptionId;
		$response = $sfConnection->query($query);
		
		return $response;
	}
	
	// get Account by Member_ed ID
	public function getAccountByMemberEDId($sfConnection, $MemberEDId) {
		$query = "SELECT Id, AccountNumber, LastName, Name FROM Account WHERE ed_member_ed_id__c =" . $MemberEDId ;
		$response = $sfConnection->query($query);
		
		return $response;	
	}
	
	// get Max ed_member_ed_id
	public function getMaxEDMember_id($sfConnection) {
		$query = "SELECT MAX(ed_member_ed_id__c) max_ed_member FROM Account";
		$response = $sfConnection->query($query);

		return $response->records[0]->any["max_ed_member"];
	}
	
	// get Personal Account by Modify Date
	public function getPersonalAccountByModifyDate($sfConnection, $mDate) {
		$crmGateway = D('SFCRMGateway');
		$gwId = $crmGateway->getGatewayId($sfConnection, 'ED');		// business unit should select "ED"
		
		$query = "SELECT 
					AccountNumber,
					CreatedDate,
					Description,
					Fax,
					HK_ID__c,
					LastName,
					FirstName,
					Location__c,
					Name,
					PersonBirthdate,
					PersonEmail,
					PersonMobilePhone,
					Phone,
					Salutation,
					ShippingStreet 
				FROM Account
				WHERE AccountNumber <> '' 
				AND ed_member_ed_id__c = null
				AND IsPersonAccount = true
				AND IsDeleted = false
				AND CRM_Gateway__c = '" . $gwId . "'"
				. " AND LastModifiedDate > " . $mDate;

		$response = $sfConnection->query($query);
	
		return $response;	
	}
	
	// get Business Account by Modify Date
	public function getBusinessAccountByModifyDate($sfConnection, $mDate) {
		$crmGateway = D('SFCRMGateway');
		$gwId = $crmGateway->getGatewayId($sfConnection, 'ED');		// business unit should select "ED"
		
		$query = "SELECT
					Contact.Account.AccountNumber,
					Contact.Account.Description,
					Contact.Account.Fax,
					Contact.Account.Location__c,
					Contact.Account.Name,
					Contact.Account.ShippingStreet,
					Contact.Account.CreatedDate,
					Email,
					LastName,
					FirstName,
					MobilePhone,
					Phone,
					Salutation
				FROM
					contact
				WHERE Contact.Account.AccountNumber <> ''
				AND Contact.Account.ed_member_ed_id__c = null
				AND Contact.Account.IsPersonAccount = false
				AND Contact.Account.IsDeleted = false
				AND Contact.Account.CRM_Gateway__c = '" . $gwId . "'"
				. " AND Contact.Account.LastModifiedDate > " . $mDate;
	
		$response = $sfConnection->query($query);
	
		return $response;
	}
	
	// upsert Accounts from Cirdb (customer)
	public function upsertAccountFromCir($sfConnection, $d) {
		$sObject = new stdClass();
		
		// check for Personal or Business Account
		for ($i = 0; $i < count($d); $i++) {
			if ($d[$i]["co"] == '') 
				{ $this->upsertPersonalAccountCir($sfConnection, $d, $i); }		// personal account
			else { $this->upsertBusinessAccountCir($sfConnection, $d, $i); }	// business account	
		}
	}
	
	// upsert a Personal Account from Cirdb
	public function upsertPersonalAccountCir($sfConnection, $d, $i) {
		$sObject = new stdClass();
		
		$crmGateway = D('SFCRMGateway');
		$gwId = $crmGateway->getGatewayId($sfConnection, 'ED');
						
		$sObject->AccountNumber = $d[$i]["cust_code"];
		$sObject->cirdb_cust_code__c = $d[$i]["cust_code"];					// Ext ID
		$sObject->Description = $d[$i]["remark"];
		$sObject->Fax = $d[$i]["fax"];
		$sObject->HK_ID__c = $d[$i]["hkid"];
		$sObject->LastName = $d[$i]["contact"];								// compulsory
		if ($d[$i]["location"] == 1) { $sObject->Location__c = '香港'; } 
			elseif ($d[$i]["location"] == 2) { $sObject->Location__c = '亞洲'; }
			elseif ($d[$i]["location"] == 3) { $sObject->Location__c = '中國'; }
			elseif ($d[$i]["location"] == 4) { $sObject->Location__c = '其他'; }
		$sObject->PersonEmail = $d[$i]["email"];
		$sObject->PersonMobilePhone = $d[$i]["telnext2"];
		if ($d[$i]["greet"] == 'Mr.' or $d[$i]["greet"] == '先生') { $sObject->Salutation = 'Mr.'; }
			elseif ($d[$i]["greet"] == 'Mrs.' or $d[$i]["greet"] == '女士') { $sObject->Salutation = 'Mrs.'; }
			elseif ($d[$i]["greet"] == 'Ms.' or $d[$i]["greet"] == 'Miss' or $d[$i]["greet"] == '小姐') { $sObject->Salutation = 'Ms.'; }
			elseif ($d[$i]["greet"] == 'Dr.') { $sObject->Salutation = 'Dr.'; }
		$sObject->Phone = $d[$i]["telnext1"];
		$sObject->ShippingStreet = $d[$i]["add1"] .PHP_EOL. $d[$i]["add2"] .PHP_EOL. $d[$i]["add3"] .PHP_EOL. $d[$i]["add4"];
		$sObject->Type = 'Customer';
		$sObject->CRM_Gateway__c = $gwId;
		
		$respone = $sfConnection->upsert('cirdb_cust_code__c', array($sObject), 'Account');

		if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Personal Account (Cir): ' . $d[$i]["cust_code"] , __FUNCTION__);}
			else { SyncLogs::writeLog('E', 'Personal Account (Cir): ' . $d[$i]["cust_code"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
	}
	
	// upsert a Business Account from Cirdb
	public function upsertBusinessAccountCir($sfConnection, $d, $i) {
		$sObject = new stdClass();
		
		$crmGateway = D('SFCRMGateway');
		$gwId = $crmGateway->getGatewayId($sfConnection, 'ED');
	
		$sObject->AccountNumber = $d[$i]["cust_code"];
		$sObject->cirdb_cust_code__c = $d[$i]["cust_code"];					// Ext ID
		$sObject->Description = $d[$i]["remark"];
		$sObject->Fax = $d[$i]["fax"];
		if ($d[$i]["location"] == 1) { $sObject->Location__c = '香港'; } 
			elseif ($d[$i]["location"] == 2) { $sObject->Location__c = '亞洲'; }
			elseif ($d[$i]["location"] == 3) { $sObject->Location__c = '中國'; }
			elseif ($d[$i]["location"] == 4) { $sObject->Location__c = '其他'; }
		$sObject->Name = $d[$i]["co"];										// compulsory
		$sObject->Phone = $d[$i]["telnext1"];
		$sObject->ShippingStreet = $d[$i]["add1"] .PHP_EOL. $d[$i]["add2"] .PHP_EOL. $d[$i]["add3"] .PHP_EOL. $d[$i]["add4"];
		$sObject->Type = 'Customer';
		$sObject->CRM_Gateway__c = $gwId;

		$response = $sfConnection->upsert('cirdb_cust_code__c', array($sObject), 'Account');
				
		// if success, update Contact
		if ($response[0]->success == true) {
			$SFContact = D('SFContact');
			$Contact = $SFContact->upsertBusinessContactCir($sfConnection, $d, $i, $respone[0]->id);
		}
		
		if ( $response[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Business Account (Cir): ' . $d[$i]["cust_code"] , __FUNCTION__);}
			else { SyncLogs::writeLog('E', 'Business Account (Cir): ' . $d[$i]["cust_code"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
	}
	
	// upsert Account from ED (member_ed)
	public function upsertAccountFromEDMember($sfConnection, $d) {
		// $sObject = new stdClass();
	
		for ($i=0; $i < count($d); $i++) {	
			$sObject = new stdClass();
			
			$sObject->AccountNumber = $d[$i]["username"];				// for eMag, no ED Account No, just the user name
			$sObject->ed_member_ed_id__c = $d[$i]["uid"];				// Ext ID
			$sObject->HK_ID__c = $d[$i]["idcard"];
			if ( $d[$i]["name"] == '' ) { $sObject->LastName = $d[$i]["username"]; }
				else { $sObject->LastName = $d[$i]["name"]; }			// complusory
			if ( $d[$i]["birthday"] <> '0000-00-00' ) { $sObject->PersonBirthDate = $d[$i]["birthday"]; } 
				else { $sObject->fieldsToNull = array("PersonBirthDate"); }
			$sObject->PersonEmail = $d[$i]["email"];
			$sObject->Phone = $d[$i]["mobile"];
			if ($d[$i]["salutation"] == 1) { $sObject->Salutation = 'Mr.'; }
				elseif ($d[$i]["salutation"] == 2) { $sObject->Salutation = 'Ms.'; }
				elseif ($d[$i]["salutation"] == 3) { $sObject->Salutation = 'Mrs.'; }
			if ( $d[$i]["income"] == 1 ) { $sOject->Income__c = '$5,000或以下'; }
				elseif ( $d[$i]["income"] == 2 ) { $sOject->Income__c = '$5,001-10,000'; }
				elseif ( $d[$i]["income"] == 3 ) { $sOject->Income__c = '$10,001-25,000'; }
				elseif ( $d[$i]["income"] == 4 ) { $sOject->Income__c = '$25,001-40,000'; }
				elseif ( $d[$i]["income"] == 5 ) { $sOject->Income__c = '$40,001-60,000'; }
				elseif ( $d[$i]["income"] == 6 ) { $sOject->Income__c = '$60,001或以上'; }
			if ( $d[$i]["marry"] == 1 ) { $sObject->Martial_Status__c = '單身'; }
				elseif ( $d[$i]["marry"] == 2 ) { $sObject->Martial_Status__c = '已婚'; }
			if ( $d[$i]["country"] == 1 ) { $sObject->Location__c = '香港'; }
				elseif ( $d[$i]["country"] == 2 ) { $sObject->Location__c = '中國'; }
				elseif ( $d[$i]["country"] == 3 ) { $sObject->Location__c = '亞洲'; }
				elseif ( $d[$i]["country"] == 4 ) { $sObject->Location__c = '亞洲'; }
				elseif ( $d[$i]["country"] == 5 ) { $sObject->Location__c = '亞洲'; }
				elseif ( $d[$i]["country"] == 6 ) { $sObject->Location__c = '亞洲'; }
				else { $sObject->Location__c = '其他'; }
			if ( $d[$i]["email_receive"] == 1 ) { $sObject->PersonHasOptedOutOfEmail = false; }
				else { $sObject->PersonHasOptedOutOfEmail = true; }
			$sObject->Type = 'Customer';

			$response = $sfConnection->upsert('ed_member_ed_id__c', array($sObject), 'Account');
			
			if ( $response[0]->success == true ) { SyncLogs::writeLog('N', 'upsert Personal Account (ED): ' . $d[$i]["username"] . ', ' . $d[$i]["uid"] , __FUNCTION__);}
				else { SyncLogs::writeLog('E', 'Personal Account (ED): ' . $d[$i]["username"] . ', ' . $d[$i]["uid"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
		}
	}
	
	// upsert Account from ED (subscription), all data from ED put into personal account
	public function upsertAccountFromEDSubscription($sfConnection, $d) {
		$sObject = new stdClass();
		
		/*
		if subscription_number is empty
			upsert by ed_subscription2011_id
		else
			search subscription_number from Account
			if exists, return Account ID and upsert by ID
			else upsert by ed_subscription2011_id
		*/
		
		$crmGateway = D('SFCRMGateway');
		$gwId = $crmGateway->getGatewayId($sfConnection, 'ED');
		
		for ($i=0; $i < count($d); $i++) {
			$sObject->AccountNumber = $d[$i]["subscription_number"];			// for new sub->generate by system, for renew->use exist no.
			$sObject->Description = $d[$i]["cname"];
			$sObject->ed_subscription2011_id__c = $d[$i]["id"];					// Ext Id
			$sObject->HK_ID__c = $d[$i]["hkid"];
			$sObject->LastName = STRTOUPPER($d[$i]["name"]);
			$sObject->PersonEmail = $d[$i]["email"];
			$sObject->Phone = $d[$i]["tel"];
			if ( $d[$i]["salutation"] == 1 ) { $sObject->Salutation = 'Mr.'; }
				elseif ($d[$i]["salutation"] == 2) { $sObject->Salutation = 'Ms.'; }
				elseif ($d[$i]["salutation"] == 3) { $sObject->Salutation = 'Mrs.'; }
			if ( $d[$i]["income"] == 1 ) { $sOject->Income__c = '$5,000或以下'; }
				elseif ( $d[$i]["income"] == 2 ) { $sOject->Income__c = '$5,001-10,000'; }
				elseif ( $d[$i]["income"] == 3 ) { $sOject->Income__c = '$10,001-25,000'; }
				elseif ( $d[$i]["income"] == 4 ) { $sOject->Income__c = '$25,001-40,000'; }
				elseif ( $d[$i]["income"] == 5 ) { $sOject->Income__c = '$40,001-60,000'; }
				elseif ( $d[$i]["income"] == 6 ) { $sOject->Income__c = '$60,001或以上'; }
			$sObject->ShippingStreet = $d[$i]["address"];
			$sObject->Type = 'Customer';
			$sObject->CRM_Gateway__c = $gwId;
			
			if ($d[$i]["subscription_number"] == '')
			{	// records without subscription number, insert new Account
				$sObject->Id = null;
				$respone = $sfConnection->upsert('ed_subscription2011_id__c', array($sObject), 'Account');
			} else {
				// records with subscription number, check record exists or not				
				$Account = D('SFAccount');
				$ExistingAccount = $Account->getAccountByAccountNumber($sfConnection, $d[$i]["subscription_number"]);
				
				if ($ExistingAccount->size == 1)
				{	// one Account found, update by Id
					$sObject->Id = $ExistingAccount->records[0]->Id;
					$respone = $sfConnection->upsert('Id', array($sObject), 'Account');
				} else {
					// else upsert by ed_subscription2011_id
					$sObject->Id = null;
					$respone = $sfConnection->upsert('ed_subscription2011_id__c', array($sObject), 'Account');
				}
			}
			if ( $respone[0]->success == true ) { SyncLogs::writeLog('N', 'upsert AccountId: ' . $respone[0]->id . ' AccountName: ' . $d[$i]["name"] , __FUNCTION__);}
				else { SyncLogs::writeLog('E', 'AccountName: ' . $d[$i]["name"] . ', Error: "' . $respone[0]->errors[0]->message . '"', __FUNCTION__); }
		}
	}
	
	// delete one or a list of Account
	public function delAccount($sfConnection, $ids) {		
		$response = $sfConnection->delete($ids);

		return $response.success;
	}
}

?>