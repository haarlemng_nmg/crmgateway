<?php

class EDPickupShopModel extends Model {
	// actual table name on ED CMS
	protected $trueTableName = 'subscription_pickup_shop_type';

	// get Pickup Shop by Modify Date
	public function getPickupShop($cutOff) {
		C('DB_CHARSET','latin1');
		
		// query
		// $condition['subscription_pickup_shop_type.status'] = 1;
		$condition['subscription_pickup_shop_type.lastmodify'] = array('EGT', $cutOff);
		
		$d = $this->db(0)
				->join('subscription_pickup_shop_category_type on subscription_pickup_shop_type.category = subscription_pickup_shop_category_type.id')
				->field('subscription_pickup_shop_type.*, subscription_pickup_shop_category_type.cname')
				->where($condition)->select();

		return $d;
	}
	
	// get Pickup Shop by ID
	public function getPickupShopById($Id) {
		C('DB_CHARSET','latin1');
		
		// query
		$condition['Id'] = $Id;
		
		$d = $this->db(0)->where($condition)->select();
		
		return $d;
	}
}

?>