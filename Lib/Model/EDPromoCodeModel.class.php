<?php

class EDPromoCodeModel extends Model {
	// actual table name
	protected $trueTableName = 'ipad2011_download_code';

	// get Promotion Code
	public function getPromoCode($cutOff) {
		C('DB_CHARSET','latin1');
		
		// query
		$condition['lastmodify'] = array('EGT', $cutOff);
		
		$d = $this->db(0)->where($condition)->select();

		return $d;
	}
	
	// get Promotion Code by Code
	public function getPromoCodeByCode($code) {
		C('DB_CHARSET','latin1');

		$condition['code'] = $code;
		
		$d = $this->db(0)->where($condition)->select();
		
		return $d;
	}
}

?>