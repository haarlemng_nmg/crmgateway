<?php

class SyncLogs
{	
	private $fileName;
	private static $_instance;
	
	public function __construct() {
		$this->fileName = APP_PATH . '/Runtime/Logs/crmgateway_log_' . time() . '.txt';
	}
	
	static private function getInstance() {
		if (!isset(self::$_instance)) {
			self::$_instance = new SyncLogs();
		}
		return self::$_instance;
	}
	
	public static function writeLog($type, $logContent, $function) {
		// write log file		
		$instance = self::getInstance();
		$time = date("Y-m-d H:i:s", time());
		
		if ($type == 'N') { $logType = 'Normal >> '; }
			elseif ($type == 'E' and $function <> NULL) { $logType = 'Exception @ ' . $function . ' >> ' ; }
			else { $logType = 'Unknown >> '; }
			
		$logContent = $time . ' - ' . $logType . $logContent . PHP_EOL;
		
		file_put_contents($instance->fileName, $logContent, FILE_APPEND | LOCK_EX);
	}
	
}
?>