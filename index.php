<?php
//定义项目名称和路径
define('APP_NAME', 'CRMGateway');
define('APP_VERSION', '1.0.7');
define('APP_LAST_UPDATED', '2014-04-23');
define('APP_PATH', './');
define('FORCEPHP_PATH', dirname(__FILE__).'/ForcePHP');
// Load Salesforce.com library
require_once (FORCEPHP_PATH .'/soapclient/SforceEnterpriseClient.php');
// 加载框架入口文件
require( "./ThinkPHP/ThinkPHP.php");

?>