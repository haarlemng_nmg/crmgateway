<?php
echo "Haarlem PHP test" . PHP_EOL;

/*
echo mb_convert_encoding('ä¹é¾ç§€èŒ‚åª (å—)ç§€å¾·æ¨“1è™', 'ISO-8859-1') . PHP_EOL;
echo mb_convert_encoding('ä¹é¾ç§€èŒ‚åª (å—)ç§€å¾·æ¨“1è™', 'UTF-8') . PHP_EOL;

echo utf8_decode('ä¹é¾ç§€èŒ‚åª (å—)ç§€å¾·æ¨“1è™') . PHP_EOL;

echo mb_detect_encoding('ä¹é¾ç§€èŒ‚åª (å—)ç§€å¾·æ¨“1è™') . PHP_EOL;

echo mb_internal_encoding() . PHP_EOL;

$lastmodify = time() - (2*60*60);
echo  date("Y-m-d H:i:s", $lastmodify) . PHP_EOL;
*/

$addr = 'line one
line two
line three
line four
line five
line six';

$r = explode('
', $addr);

var_dump($r);

echo $r[0] . PHP_EOL;
echo $r[1] . PHP_EOL;
echo $r[2] . PHP_EOL;
$last = $r[3];
for ($i = 4; $i<=count($r); $i++) {
	$last = $last .' '. $r[$i];
}
echo $last . PHP_EOL;

echo 'count: ' . count($r) . PHP_EOL;

/*
date_default_timezone_set('UTC');
echo 'Time (UTC): ' . date('F j, Y, g:i a', time()) . PHP_EOL;

echo date("Y", time()) . date("m", time()) . PHP_EOL;

date_default_timezone_set('Asia/Hong_Kong');
echo 'Time (HK): ' . date('F j, Y, g:i a', time()). PHP_EOL;

date_default_timezone_set('Europe/London');
echo 'Time (London): ' . date('F j, Y, g:i a', time()) . PHP_EOL;

date_default_timezone_set('Canada/Atlantic');
echo 'Time (Canada/Atlantic): ' . date('F j, Y, g:i a', time()) . PHP_EOL;

date_default_timezone_set('Asia/Hong_Kong');

$cutoff = time();
echo '$cuoff: ' . $cutoff . PHP_EOL;													// 1395214568
echo 'strtotime($cutoff): ' . strtotime($cutoff) . PHP_EOL;								// <blank>

echo 'Formated $cutoff: ' . date('Y-m-d', $cutoff) . PHP_EOL;							// 2014-03-19
echo 'Formated strtotime($cutoff): ' . date('Y-m-d', strtotime($cutoff)) . PHP_EOL;		// 1970-01-01

echo 'st(14T): ' . strtotime('2014-02-14T09:00:00+08:00') . PHP_EOL;					// 1392339600
echo 'st(14T) back: ' . date('Y-m-d', 1392339600 ) . PHP_EOL;							// 2014-02-14
echo 'date(): ' . date('Y-m-d H:i:s', 1334320479) . PHP_EOL;

echo 'st(14): ' . strtotime('2014-02-14 09:00:00') . PHP_EOL;							// 1392339600
echo 'st(14T) back: ' . date('Y-m-d\TH:i:s\+08:00', 1392339600 ) . PHP_EOL;				// 2014-02-14T09:00:00+08:00
echo 'year: ' . date('Y', strtotime('2014-02-14 09:00:00')) . PHP_EOL;					// 2014
echo 'month: ' . date('m', strtotime('2014-02-14 09:00:00')) . PHP_EOL;					// 02

$a = 'test time';

test();

function test()
{
	global $a;
	echo '$a: ' . $a . PHP_EOL;
}

*/

?>
